import os



class BasePath:
    pass
class ProjectPath(BasePath):
    #项目路径
    ROOT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    #测试用例路径
    DATA_PATH  = os.path.join(ROOT_PATH,'data')
    #配置文件路径
    CONFIG_PATH = os.path.join(ROOT_PATH,'settings')
    #测试用例路径
    CASE_PATH = os.path.join(ROOT_PATH, 'test_case')
    #测试报告路径
    REPORT_PATH = os.path.join(ROOT_PATH, 'report')

    #日志地址
    LOG_PATH = os.path.join(ROOT_PATH, 'logs')

    # #如果没有文件夹，自动创建
    # if not os.path.exists(REPORT_PATH):
    #     os.mkdir(REPORT_PATH)


p_path = ProjectPath()

if __name__ == '__main__':
    print(p_path.ROOT_PATH)
