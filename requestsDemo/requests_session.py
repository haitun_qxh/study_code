#之前使用cookies,每次都需要手动添加，比较麻烦，
# session相当于Jmeter的会话管理器，会自动管理cookie
#动态管理cookies,集中管理所有资源

import requests

url_login = "http://127.0.0.1:5000/login"
url = "http://127.0.0.1:5000/"

#1.创建会话对象
session = requests.Session()

#2.会话访问后，自动保存session,必须进行访问,每次会保存cookie属性到session对象，每次访问会更新
res = session.get(url_login)

#3.访问需要cookie的url，无需再进行手动传入
res1 = session.get(url)

print(res1.text)