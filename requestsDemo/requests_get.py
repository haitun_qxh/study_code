#不带参数的get请求

import requests
url = " http://127.0.0.1:5000/"
res = requests.get(url)

#返回状态码
print(res.status_code)
#返回收据文本，html等
print(res.text)

#接收响应二进制，图片，视频等
print(res.content)

#json格式响应，字典，
# 必须是json格式才可以，不是JSon格式会报错
print(res.json())