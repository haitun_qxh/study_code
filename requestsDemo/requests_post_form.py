import requests

url = " http://127.0.0.1:5000/login"
data = {'username':"wwj",'pwd':"123456"}

#1.from表单格式的数据也是key=value键值对，需要使用data来接收
#2.可以不用设置请求头,默认请求头就是form表单格式

res = requests.post(url,data=data)

print(res.status_code)
print(res.headers)
print(res.text)
print(res.json())
print(res.cookies)