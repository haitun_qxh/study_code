from pymysql.cursors import DictCursor

from common.ConfigUtil import config
from common.DBUtil import DBUtil


class MyDBHandler(DBUtil):
    '''直接使用DBUtil,会导致很多读取配置的操作'''
    def __init__(self,**kw):
        super().__init__(
            host= config.read('db','host'),
            port= eval(config.read('db','port')),
            user= config.read('db','user'),
            password=config.read('db','password'),
            charset=config.read('db','charset'),
            database=config.read('db','database'),
            cursorclass = DictCursor,
            **kw
        )

if __name__ == '__main__':
    db = MyDBHandler()
    user = db.query("SELECT * FROM member WHERE id = 10;")
    print(user)


