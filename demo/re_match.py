import re

#正则表达式学习
#match匹配开头
#第一个参数是正则表达式，第二个是字符串,
# r是防止其中有转义
#group是分组
r = re.match(r'hhh','hhh123')
print(r)
print(r.group())
print(r.group(0))

#2. \w表示字母，\d表示数字，都匹配单个字符
r1 = re.match(r'\w\d','h9hh123')   #匹配两个字母。第一个和第二个
print(r1.group())

#3.*匹配任意字符0或者无数次
r2 = re.match(r'\w*','')   #匹配两个字母。第一个和第二个
print(r2.group())
#4.匹配任意字符1或者无数次
r3 = re.match(r'\w+','h')   #匹配两个字母。第一个和第二个
print(r3.group())


#5.?表示次数.0或者1次，非贪婪模式

r4 = re.match(r'\w*？\d','hhhh1')   #匹配两个字母。第一个和第二个
print(r4.group())





