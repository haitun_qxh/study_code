import re
#直接在字符串中匹配

#之间找数字，
r = re.search('\d','hh2kk3')
#匹配第一个
print(r.group())
#{1,3}匹配1到3次
r1 = re.search('h{1,3}','hhhh2kk3')
print(r1.group())

#[2k3]匹配括号中任意元素
r2 = re.search('h[def]','hhhhdef')
print(r2.group())

#？是非贪婪模式
pattern = r'\*(.+?)\*'

a = re.search(pattern,'{"username":"*phone*","pwd":"*pwd*"}')
print(a.group(0))  #0是带匹配符
print(a.group(1))   #1不带匹配符

my_string  = '{"username":"*phone*","pwd":"*pwd*"}'
#替换
b = re.sub(pattern,'12288889999',my_string,1)  #替换第一个
c = re.sub(pattern,'12288889999',my_string,0)  #替换全部
print(b)
print(c)

