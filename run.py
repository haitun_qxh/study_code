
import os
import unittest
from datetime import datetime

from settings.constant import p_path

suite = unittest.TestSuite()
loader = unittest.defaultTestLoader

suite = loader.discover(p_path.CASE_PATH,
                        pattern="test*.py",
                        top_level_dir=None)

print(p_path.CASE_PATH)
report_name = datetime.now().strftime("%Y-%m-%d-%M-%S")
print(report_name)

report_file = os.path.join(p_path.REPORT_PATH,report_name+'.html')
print(report_file)


if __name__ == '__main__':
    with open(report_file,'wb') as f:
        runner = unittest.TextTestRunner()
        runner.run(suite)