import pymysql
from pymysql.cursors import DictCursor

conn = pymysql.connect(
    host='120.78.128.25',
    port = 3306,
    user = 'future',
    password = '123456',
    charset = 'utf8', #不能写成utf-8
    database = 'futureloan',
    cursorclass = DictCursor
)


#建立游标
cursor = conn.cursor()

cursor.execute("SELECT * FROM member WHERE id= 10;")

#获取数据，获取一条记录
res = cursor.fetchone()
conn.commit()
print(res)

#获取数据获取全部数据
# res1 = cursor.fetchall()
# print(res1)

#关闭
cursor.close()
conn.close()


