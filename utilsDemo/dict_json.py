#字典转换为json

import json
#字典
user_info = {'name':'wwj','age':13,'pwd':None,'sex':'女'}
#单引号自动转换为双引号
#字典转换为json
#None转化为null
#中文字符问题，ensure_ascii=True
user_json = json.dumps(user_info,ensure_ascii=False)
print(user_json)

print(type(user_info))
print(type(user_json))

#json转换为字典
user_info_new = json.loads(user_json)
print(user_info_new)
print(type(user_info_new))

#json字符串中使用双引号，而不是单引号，不然转换字典的时候会报错
