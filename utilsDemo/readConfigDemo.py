#引入解析文件配置库
from configparser import ConfigParser
config = ConfigParser()
#读取配置文件
config.read("../settings.ini",encoding='utf-8')

# config.get(section,option)
print(config.get("students","name"))
print(config.get("excel","file_path"))
#字典方式读取
print(config['students']['age'])

#之前解析出的都是字符串，如果想解析出原版的格式
ma = config.getboolean("students","married")
print(type(ma))
print(ma)
#自己实现，封装一个类

