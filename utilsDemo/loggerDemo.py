#日志文件
import logging
# 初始化logger,快读配置
logger = logging.basicConfig(level='DEBUG')
def log(a,b,c):
    #主体功能信息
    logger.info("-----info-------------")
    #调试信息
    logger.debug("----debug------------")
    #警告信息
    logger.warning("-----warning---------")
    #报错信息
    logger.error("--------error------------")
    return a+b == c

print(log(1,2,3))

# 运行结果：
# WARNING:root:-----warning---------
# ERROR:root:--------error------------

# 为何没显示info和debug

