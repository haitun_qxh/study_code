# _*_ coding: utf-8 _*_
import logging
import os.path
import time


class LoggerUtil():
    """
    1.收集器
    2.设置收集器的界别
    3.初始化处理器
    4.处理器的级别
    5.添加处理器
    6.初始化格式
    7.设置处理器格式

    """

    def __init__(self, logger):
        """
        指定保存日志的文件路径，日志级别，以及调用文件
            将日志存入到指定的文件中
        :param logger:
        """
        # 1.创建一个logger收集器
        self.logger = logging.getLogger(logger)
        # 2.设置处理器的界别
        self.logger.setLevel(logging.DEBUG)


        # 3.创建一个handler，用于写入日志文件
        rq = time.strftime('%Y%m%d%H%M', time.localtime(time.time()))
        log_path = os.path.dirname(os.getcwd()) + '/logs/'
        log_name = log_path + rq + '.log'

        fh = logging.FileHandler(log_name,encoding='utf-8')
        fh.setLevel(logging.INFO)

        # 4.再创建一个handler，用于输出到控制台
        ch = logging.StreamHandler()
        #5.处理器级别，如果不手动配置，就会默认warning
        ch.setLevel(logging.INFO)

        # 6.定义handler的输出格式
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)

        # 7.给logger添加handler
        self.logger.addHandler(fh)
        self.logger.addHandler(ch)

    def getlog(self):
        #Logger().info()
        return self.logger


if __name__ == '__main__':
    logger = LoggerUtil("demo")
    logger.getlog().warning("----loh--------")