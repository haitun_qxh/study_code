from selenium import webdriver

from utilsDemo.LoggerUtil import Logger
import time

mylogger = Logger(logger='TestMyLog').getlog()
class TestMyLog():
    def log_01(self):
        driver = webdriver.Chrome()
        mylogger.info("打开浏览器")
        driver.maximize_window()
        mylogger.info("最大化浏览器窗口。")

        driver.get("https://www.baidu.com")
        mylogger.info("打开百度首页1111。")
        time.sleep(1)
        mylogger.info("暂停一秒。")
        driver.quit()
        mylogger.info("关闭并退出浏览器。")

log = TestMyLog()
log.log_01()