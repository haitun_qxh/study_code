#日志文件
import logging

#什么时候用logging,快速收集器RootLogger,什么时候使用自定义的logging.getLogger()
# logging学习时候，调试时候，快速写日志
#2成熟框架，或者生产代码，还是要用logging.getLogger()严格定义格式等级等





#1.logger日志收集器
#快速初始化logging.Logger('demo',level=0)
logger = logging.getLogger("日志收集器")
#2.设置日志收集器的级别
logger.setLevel("DEBUG")
#3.控制台显示.日志处理器
console_handler = logging.StreamHandler()

#4.初始化文件处理器
file_handler = logging.FileHandler('demo.log',encoding='utf-8')

#5.日志处理器添加处理器
logger.addHandler(console_handler)
logger.addHandler(file_handler)
#6.设置输出格式
console_fmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(filename)s - %(lineno)s- %(message)s')
console_handler.setFormatter(console_fmt)

file_fmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(filename)s - %(lineno)s- %(message)s')
file_handler.setFormatter(file_fmt)

logger.warning("测试一下log----------")





