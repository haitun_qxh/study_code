from flask import Flask, jsonify,request,Response

app = Flask(__name__)

@app.route("/",methods=['GET','POST'])
def index():
    if not request.cookies.get('user'):
        return 'not login'
    return jsonify({"msg":"hello"})

@app.route('/login',methods=['GET','POST'])
def login():
    resp = Response("login  success")
    #登录后设置cookie,可以在请求的响应中查看，再次登录可以直接成功
    resp.set_cookie('user','wwj')
    return resp

app.run(debug = True)
