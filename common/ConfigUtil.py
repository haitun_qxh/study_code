#封装一个配置文件类
import os
from configparser import ConfigParser, NoSectionError, NoOptionError

from settings.constant import p_path


class ConfigUtil:
    def __init__(self,filename):
        self.filename = filename
        config = ConfigParser()
        config.read(filename,encoding='utf-8')
        self.config = config


    def read(self,section,option):
        """读取配置文件"""
        try:
            return self.config.get(section,option)
        except NoSectionError:
            print("没有这个section------")
        except NoOptionError:
            print("没有这个option-------")

    def write(self,section,option,value):
        """写操作"""
        if self.config.has_option(section):
            self.section.set(section,option,value)
            with open(self.filename,'w',encoding='utf-8') as f:
                self.config.write(f)

    def get_list(self,section,option):
        option_str = self.read(section,option)
        if isinstance(eval(option_str),list):
            return eval(option_str)
        return None


class ConfigHander(ConfigParser):
    def __init__(self,filename):
        super().__init__()
        self.read(filename,encoding='utf-8')

    def get(self,section,option):
        try:
            return super().get(section,option)
        except:
            pass



config = ConfigUtil(os.path.join(p_path.CONFIG_PATH,'config.ini'))


# config = ConfigUtil("../settings/config.ini")
if __name__ == '__main__':
    print(os.path.join(p_path.CONFIG_PATH, 'config.ini'))

    # print(config.read('excel','file_name'))

