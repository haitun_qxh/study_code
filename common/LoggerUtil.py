import logging
import os

from common.ConfigUtil import ConfigUtil
from settings.constant import p_path


class LoggerUtil(logging.Logger):
    def __init__(self,
                 name,
                 level = 'INFO',
                 file_name ="../logs/demo.log" ,
                 handler_level = 'INFO',
                 fmt="%(asctime)s - %(name)s - %(levelname)s - %(filename)s - %(lineno)s"
                 ):
        super().__init__(name,level=level)
        console_handler = logging.StreamHandler()
        file_handler  = logging.FileHandler(os.path.join(p_path.LOG_PATH, 'log.demo'),encoding='utf-8')
        #设置hander级别
        console_handler.setLevel(handler_level)
        file_handler.setLevel(handler_level)

        #设置format格式
        handler_format = logging.Formatter(fmt)
        #添加格式化
        console_handler.setFormatter(handler_format)
        file_handler .setFormatter(handler_format)
        #
        #7. 给logger添加handler
        self.addHandler(console_handler)
        self.addHandler(file_handler)

logger = LoggerUtil("日志信息啊")


# logger = LoggerUtil(
#                       conf.read('logger','name'),
#                       level=conf.read('logger','level'),
#                      file_name = conf.read('logger','file_name'),
#                      handler_level= conf.read('logger','handler_level')
#                      )

#
#
if __name__ == '__main__':
    logger.info("日志类文件改造-------------")