import json
import os
import unittest

from ddt import ddt, data


from common.ConfigUtil import config
from common.ExcelUtil import ExcelUtil
from common.RequestsUtil import RequestsUtil
from func.helper import mk_phone
from middle_ware.db_hander import MyDBHandler
from settings.constant import p_path


@ddt
class TestRegister(unittest.TestCase):
    #读取配置文件
    file_name = config.read('excel', 'file_name')
    file_path = os.path.join(p_path.DATA_PATH, file_name)
    #EXCEL的sheet地址
    sheet_name = config.read('excel', 'register_sheet')
    #Url地址
    url = config.read('http','base_url')
    headers = config.read('http','headers')
    xls = ExcelUtil(file_path, sheet_name)
    test_data = xls.read()
    test_title = xls.title()
    result_index = test_title.index('result')

    # test_data = ExcelUtil(file_path).read(sheet_name)
    # test_title = ExcelUtil(file_path).title(sheet_name)


    @classmethod
    def setUpClass(cls):
        cls.req = RequestsUtil()

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.db = MyDBHandler()

    def tearDown(self):
        self.db.close()

    @data(*test_data)
    def test_register(self,test_info):

        #替换参数

        if "*exist_phone*" in test_info['data']:
            user = self.db.query("SELECT * FROM member;")
            test_info['data'] = test_info['data'].replace("*exist_phone*", user['mobile_phone'])
        elif '#phone#' in test_info['data']:
            #正常测试用例，生成一个手机号码
            while True:
                phone = mk_phone()
                user = self.db.query("SELECT * FROM member WHERE mobile_phone = %s;",
                                     args=[phone,])
                if not user:
                    break

            test_info['data'] = test_info['data'].replace("#phone#", user['mobile_phone'])

        res = self.req.json(test_info['method'],
                            self.url+test_info['url'],
                            json=eval(test_info['data']),
                            headers = eval(self.headers)


        )
        print(res)
        #回写测试数据
        try:

            #全部断言
            self.assertEqual(json.loads(test_info['expected']), res)
            self.xls.write(
                            test_info['case_id']+1,
                            self.result_index+1,
                            'pass')
        except AssertionError as e:
            self.xls.write(
                            test_info['case_id'] + 1,
                            self.result_index+1,
                            'failed')
            raise e

#写完代码空一行，不然还在方法内运行容易报错


