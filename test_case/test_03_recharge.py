import json
import os
import unittest

from ddt import ddt, data


from common.ConfigUtil import config
from common.ExcelUtil import ExcelUtil
from common.RequestsUtil import RequestsUtil
from func.helper import mk_phone, get_token
from middle_ware.db_hander import MyDBHandler
from settings.constant import p_path


@ddt
class TestRecharge(unittest.TestCase):
    #读取配置文件
    file_name = config.read('excel', 'file_name')
    file_path = os.path.join(p_path.DATA_PATH, file_name)
    #EXCEL的sheet地址
    sheet_name = config.read('excel', 'recharge_sheet')
    #Url地址
    url = config.read('http','base_url')
    headers = eval(config.read('http','headers'))
    xls = ExcelUtil(file_path, sheet_name)
    test_data = xls.read()
    test_title = xls.title()
    result_index = test_title.index('result')

    # test_data = ExcelUtil(file_path).read(sheet_name)
    # test_title = ExcelUtil(file_path).title(sheet_name)


    @classmethod
    def setUpClass(cls):
        cls.req = RequestsUtil()
        #登录并获取token
        cls.user_info = get_token()
        cls.headers['Authorization'] = 'Bearer ' + cls.user_info['token']
        print(cls.headers['Authorization'])

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.db = MyDBHandler()

    def tearDown(self):
        self.db.close()

    @data(*test_data)
    def test_recharge(self,test_info):

        if "#member_id#" in test_info['data']:
            test_info['data'] = test_info['data'].replace(
                "#member_id#",str(self.user_info["member_id"]))

        print("test_data"+test_info['data'])


        if "*wrong_member_id*" in test_info['data']:
            test_info['data'] = test_info['data'].replace(
                "*wrong_member_id*", str(self.user_info["member_id"]+1))


        db_user_info = self.db.query('SELECT * FROM member WHERE id =%s;',
                               args=[self.user_info['member_id']])

        print(db_user_info)

        amount = db_user_info['leave_amount']

        res = self.req.json(test_info['method'],
                            self.url + test_info['url'],
                            json=eval(test_info['data']),
                            headers= self.headers)
        print(res)
        self.assertEqual(test_info['expected'],res['code'])

        if res['code'] == 0:

            self.assertEqual(
                res['data']['leave_amount']-json.loads(test_info['data'])['amount'],amount
                             )


