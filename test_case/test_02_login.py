import os
import unittest

from ddt import ddt, data
from rest_framework.utils import json

from common.ConfigUtil import config
from common.ExcelUtil import ExcelUtil
from common.LoggerUtil import logger
from common.RequestsUtil import RequestsUtil
from middle_ware.db_hander import MyDBHandler
from settings.constant import p_path


@ddt
class TestLogin(unittest.TestCase):
    #读取配置文件
    file_name = config.read('excel', 'file_name')
    file_path = os.path.join(p_path.DATA_PATH, file_name)
    #EXCEL的sheet地址
    sheet_name = config.read('excel', 'login_sheet')
    #Url地址
    url = config.read('http','base_url')
    headers = config.read('http','headers')
    # test_data = ExcelUtil(file_path).read(sheet_name)
    xls = ExcelUtil(file_path,sheet_name)
    test_data = xls.read()
    test_title = xls.title()
    result_index = test_title.index('result')

    @classmethod
    def setUpClass(cls):
        cls.req = RequestsUtil()
        cls.log = logger

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.db = MyDBHandler()
    def tearDown(self):
        self.db.close()

    @data(*test_data)
    def test_login(self,test_info):

        res = self.req.json(test_info['method'],
                            self.url+test_info['url'],
                            json=eval(test_info['data']),
                            headers = eval(self.headers)

        )

        # 回写测试数据
        try:
            self.assertEqual(test_info['expected'], res['msg'])
            self.log.info("断言成功")

            self.xls.write(
                test_info['case_id'] + 1,
                self.result_index + 1,
                'pass')
        except AssertionError as e:
            self.xls.write(
                test_info['case_id'] + 1,
                self.result_index + 1,
                'failed')
            self.log.error("断言失败")
            raise e


