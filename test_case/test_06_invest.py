import json
import os
import unittest

from ddt import ddt, data


from common.ConfigUtil import config
from common.ExcelUtil import ExcelUtil
from common.RequestsUtil import RequestsUtil
from func.helper import mk_phone, get_token, replace_label
from middle_ware.db_hander import MyDBHandler
from settings.constant import p_path


@ddt
class TestInvest(unittest.TestCase):
    #读取配置文件
    file_name = config.read('excel', 'file_name')
    file_path = os.path.join(p_path.DATA_PATH, file_name)
    #EXCEL的sheet地址
    sheet_name = config.read('excel', 'invest_sheet')
    #Url地址
    url = config.read('http','base_url')
    headers = eval(config.read('http','headers'))
    xls = ExcelUtil(file_path, sheet_name)
    test_data = xls.read()
    test_title = xls.title()
    result_index = test_title.index('result')

    # test_data = ExcelUtil(file_path).read(sheet_name)
    # test_title = ExcelUtil(file_path).title(sheet_name)


    @classmethod
    def setUpClass(cls):
        cls.req = RequestsUtil()
        #登录并获取token
        cls.user_info = get_token()

        cls.headers['Authorization'] = 'Bearer ' + cls.user_info['token']
        print(cls.headers['Authorization'])

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.db = MyDBHandler()

    def tearDown(self):
        self.db.close()

    @data(*test_data)
    def test_invest(self,test_info):
        # 1.登录，拿到token_id,作为接下来的请求信息
        # 2.创建标
        # 3.审核标
        # 4.投资

        test_info['data'] = replace_label(test_info['data'])
        print(test_info['data'])

        # # 判断超额情况
        # if "*above_balance*" in test_info['data']:
        #     # db_user_info = self.db.query('SELECT * FROM member WHERE id =%s;',
        #     #                              args=[self.user_info['member_id']])
        #     test_info['data'] = test_info['data'].replace(
        #         "*above_balance*", )


        user_info = self.db.query('SELECT * FROM member WHERE id = %s',
                                  args=[self.user_info['member_id']])
        before_money = user_info['leave_amount']

        res = self.req.json(test_info['method'],
                            self.url + test_info['url'],
                            json=eval(test_info['data']),
                            headers=self.headers)
        print(res)
        self.assertEqual(test_info['expected'], res['code'])
        if res['code'] == 0 and test_info['url'] == '/member/invest':
            user_info_after = self.db.query('SELECT * FROM member WHERE id = %s',
                                  args=[self.user_info['member_id']])
            after_money = user_info_after['leave_amount']
            self.assertEqual(before_money-json.loads(test_info['data'])['amount'],
                             after_money)




